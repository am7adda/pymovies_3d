# -*- coding: utf-8 -*-
"""
Created on Thu Apr 23 16:47:52 2020

@author: guillaume
"""


import cartopy.crs as ccrs

import cartopy.feature as cfeature
import matplotlib.pyplot as plt
import numpy as np 

import matplotlib 
import folium

def carte_france(figsize=(18, 18)):
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree())
    # ax.set_extent([-3, 0, 46, 49])

    ax.add_feature(cfeature.OCEAN.with_scale('50m'))
    ax.add_feature(cfeature.RIVERS.with_scale('50m'))
    ax.add_feature(cfeature.BORDERS.with_scale('50m'), linestyle=':')
    ax.set_title('France');
    return ax

# def affichage_carte_france(nombre_de_categories,labels,Lat_moy_repmat,Lon_moy_repmat,Sa_moy):
#     couleurs = plt.cm.jet(np.linspace(0,1,nombre_de_categories))
#     for x in range(nombre_de_categories):
#         index_lab  = np.where(labels==x)
#         ax = carte_france()
#         ax.scatter(Lon_moy_repmat[index_lab],Lat_moy_repmat[index_lab],s=(Sa_moy[index_lab]),color=couleurs[x])
 

# def save_map_html (nombre_de_categories,labels,Lat_moy_repmat,Lon_moy_repmat,Sa_moy):
#     couleurs = plt.cm.jet(np.linspace(0,1,nombre_de_categories))
#     for x in range(nombre_de_categories):
#         couleur_hexa = matplotlib.colors.to_hex( couleurs[x] , keep_alpha=True)
#         index_lab  = np.where(labels==x)
#         map_osm = folium.Map(location=[Lat_moy_repmat[0,0],Lon_moy_repmat[0,0]])
#         for i in range(len(index_lab[0])):
            
        
#             folium.CircleMarker([Lat_moy_repmat[index_lab[0][i],index_lab[1][i]],Lon_moy_repmat[index_lab[0][i],index_lab[1][i]]],radius=(Sa_moy[index_lab[0][i],index_lab[1][i]]),color = couleur_hexa,fill=True,fill_opacity=1).add_to(map_osm)
#       # Ajouter le chemin  dans map_osm.save sinon la page html s'enregistre dans le dossier contenant ei_survey
#         map_osm.save('carte_%d.html'%(x))
def affichage_carte_france(nombre_de_categories,labels,Lat_moy,Lon_moy,Sa_moy,path = None):
    couleurs = plt.cm.jet(np.linspace(0,1,nombre_de_categories))
    for x in range(nombre_de_categories):
        
        
        ax = carte_france()
        ax.scatter(Lon_moy,Lat_moy,s=Sa_moy[x],color=couleurs[x])
        if path is not None : 
            plt.savefig("%s/map_label_%d.png" %(path,x))
                   

def save_map_html (nombre_de_categories,labels,Lat_moy,Lon_moy,Sa_moy,path=None):
    couleurs = plt.cm.jet(np.linspace(0,1,nombre_de_categories))
    for x in range(nombre_de_categories):
        couleur_hexa = matplotlib.colors.to_hex( couleurs[x] , keep_alpha=True)
        
        map_osm = folium.Map(location=[Lat_moy[0],Lon_moy[0]])
        for i in range(len(Lat_moy)):
            
        
            folium.CircleMarker([Lat_moy[i],Lon_moy[i]],radius=(Sa_moy[x][i]),color = couleur_hexa,fill=True,fill_opacity=1).add_to(map_osm)
      # Ajouter le chemin  dans map_osm.save sinon la page html s'enregistre dans le dossier contenant ei_survey
        
        if path is not None :
            map_osm.save('%s/carte_%d.html'%(path,x))
        else :
            map_osm.save('carte_%d.html'%(x))
