# -*- coding: utf-8 -*-
"""
Created on Mon Jun 8 

@author: Laurent Berger
"""


"""
Export integrated data in memory to Sonar-Netcdf4 file format
Since GridGroup is not yet available in the format integrated data is stored in BeamGroup with samplingInterval set to the vertical size of the integration cell


"""

import os
import tempfile as tmp

import netCDF4 as nc
import numpy as np
import datetime

import pyat.core.xsf.xsf_constants as xsf

def save_integrated_SonarNetCdf(sounder,Time,Sv,Lat,Lon,Depth,path,survey_name):
    
    nb_transduc=sounder.m_numberOfTransducer
    nb_pings=len(Sv)
        
    filename = '%s%s.nc' % (path,survey_name)
    print(f"creating fake xsf file {filename}")

    with nc.Dataset(filename, mode="w") as file:
        # create root Node
        root_structure = xsf.RootGrp()
        root = root_structure.create_group(file)
        root_structure.create_crs(root)
               
                                     
        sonar_structure = xsf.SonarGrp()
        sonar = sonar_structure.create_group(root)   
        
        frequencyEnv=[]
        absorptionEnv=[]
        
        trans_posx=np.empty(nb_transduc)
        trans_posy=np.empty(nb_transduc)
        trans_posz=np.empty(nb_transduc)
        trans_orx=np.empty(nb_transduc)
        trans_ory=np.empty(nb_transduc)
        trans_orz=np.empty(nb_transduc)
            
        for itr in range(nb_transduc):
                trans = sounder.GetTransducer(itr)
                platform_trans=trans.GetPlatform()
                trans_posx[itr]=platform_trans.along_ship_offset
                trans_posy[itr]=platform_trans.athwart_ship_offset
                trans_posz[itr]=platform_trans.depth_offset
                trans_orx[itr]=trans.m_transFaceAlongAngleOffsetRad
                trans_ory[itr]=trans.m_transFaceAthwarAngleOffsetRad
                trans_orz[itr]=trans.m_transRotationAngleRad
                
                nb_beams=trans.m_numberOfSoftChannel
                beam_structure = xsf.BeamGroup1Grp()
                beam = beam_structure.create_group(sonar,f"Beam_group{itr+1}")
                beam_structure.create_dimension(
                    beam,
                    {
                        xsf.BeamGroup1Grp.BEAM_DIM_NAME: nb_beams,
                        xsf.BeamGroup1Grp.SUBBEAM_DIM_NAME: 1,
                        xsf.BeamGroup1Grp.TX_BEAM_DIM_NAME: nb_beams,
                        xsf.BeamGroup1Grp.PING_TIME_DIM_NAME: nb_pings,
                    },
                )
                #prepare variables
                frequency=np.empty(nb_beams)
                absorption=np.empty(nb_beams)
                alongAngleSensitivity=np.empty(nb_beams)
                athwartAngleSensitivity=np.empty(nb_beams)
                beamwidthalong=np.empty(nb_beams)
                beamwidthathwart=np.empty(nb_beams)
                steeringAlong=np.empty(nb_beams)
                steeringAthwart=np.empty(nb_beams)
                beamType=np.empty(nb_beams)
                equivalentBeamAngle=np.empty(nb_beams)
                beamGain=np.empty(nb_beams)
                saCorrection=np.empty(nb_beams)
                bandWidth=np.empty(nb_beams)
                startFrequency=np.empty(nb_beams)
                stopFrequency=np.empty(nb_beams)
                transmitPower=np.empty(nb_beams)
                samplingInterval=trans.m_timeSampleInterval
                pulseDuration=trans.m_pulseDuration
                pulseShape=trans.m_pulseShape
                
                for ibeam in range(nb_beams):
                    softChan = trans.getSoftChannelPolarX(ibeam)
                    frequency[ibeam]=softChan.m_acousticFrequency
                    frequencyEnv.append(frequency[ibeam])
                    absorption[ibeam]=softChan.m_absorptionCoef/10000000
                    absorptionEnv.append(absorption[ibeam])
                    alongAngleSensitivity[ibeam]=softChan.m_beamAlongAngleSensitivity
                    athwartAngleSensitivity[ibeam]=softChan.m_beamAthwartAngleSensitivity
                    beamwidthalong[ibeam]=softChan.m_beam3dBWidthAlongRad
                    beamwidthathwart[ibeam]=softChan.m_beam3dBWidthAthwartRad
                    steeringAlong[ibeam]=softChan.m_mainBeamAlongSteeringAngleRad
                    steeringAthwart[ibeam]=softChan.m_mainBeamAthwartSteeringAngleRad
                    beamType[ibeam]=softChan.m_beamType
                    equivalentBeamAngle[ibeam]=softChan.m_beamEquTwoWayAngle
                    beamGain[ibeam]=softChan.m_beamGain
                    saCorrection[ibeam]=softChan.m_beamSACorrection
                    bandWidth[ibeam]=softChan.m_bandWidth
                    startFrequency[ibeam]=softChan.m_startFrequency
                    stopFrequency[ibeam]=softChan.m_endFrequency
                    transmitPower[ibeam]=softChan.m_transmissionPower
                    
        
                variable = beam_structure.create_ping_time(beam)
                variable[:] = np.array(Time, dtype='datetime64')
                
                Sv[np.where(Sv<=-150)] = np.nan
                
                lambda_ac=sounder.m_soundVelocity/frequency
                pulseeffective=(pulseDuration*10**(2*saCorrection/10))/1000000
                slplusvr=10*np.log10(transmitPower*lambda_ac**2*sounder.m_soundVelocity/(32*(np.pi)**2*pulseeffective))+equivalentBeamAngle+2*beamGain
                Pr=Sv[:,:,itr]-slplusvr-20*np.log10(Depth)-2*(absorption)*Depth
                
                variable = beam_structure.create_backscatter_r(beam)
                for iping in range(0,nb_pings):
                    for ibeam in range(0,nb_beams):
                        variable[iping,ibeam,0] = np.float32(Pr[iping])
        
                variable = beam_structure.create_echoangle_major_sensitivity(beam)
                variable[:] = alongAngleSensitivity
                variable = beam_structure.create_echoangle_minor_sensitivity(beam)
                variable[:] = athwartAngleSensitivity
                variable = beam_structure.create_beamwidth_transmit_major(beam)
                variable[:] =beamwidthalong*180/np.pi
                variable = beam_structure.create_beamwidth_transmit_minor(beam)
                variable[:] = beamwidthathwart*180/np.pi
                variable = beam_structure.create_beamwidth_receive_major(beam)
                variable[:] = beamwidthalong*180/np.pi
                variable = beam_structure.create_beamwidth_receive_minor(beam)
                variable[:] = beamwidthathwart*180/np.pi
                variable = beam_structure.create_rx_beam_rotation_phi(beam)
                variable[:] =steeringAlong*180/np.pi
                variable = beam_structure.create_rx_beam_rotation_theta(beam)
                variable[:] = steeringAthwart*180/np.pi
                variable = beam_structure.create_rx_beam_rotation_psi(beam)
                variable[:] = np.zeros(nb_beams)
                variable = beam_structure.create_tx_beam_rotation_phi(beam)
                variable[:] = steeringAlong*180/np.pi
                variable = beam_structure.create_tx_beam_rotation_theta(beam)
                variable[:] = steeringAthwart*180/np.pi
                variable = beam_structure.create_tx_beam_rotation_psi(beam)
                variable[:] = np.zeros(nb_beams)
                variable = beam_structure.create_beam_stabilisation(beam)
                if sounder.m_isMultiBeam:
                    variable[:] = np.ones(nb_pings)
                else:
                    variable[:] = np.zeros(nb_pings)
                variable = beam_structure.create_beam_type(beam)
                variable[:] = beamType
                variable = beam_structure.create_equivalent_beam_angle(beam)
                variable[:] = 10**(equivalentBeamAngle/10)
                variable = beam_structure.create_non_quantitative_processing(beam)
                variable = np.zeros(nb_pings,dtype=bool)
                variable = beam_structure.create_sample_interval(beam)
                #variable[:] = samplingInterval/1000000 #sampling interval of raw data
                variable[:] = 2*(Depth[:,1]-Depth[:,0])/sounder.m_soundVelocity
                variable = beam_structure.create_sample_time_offset(beam)          
                variable[:] = np.zeros(nb_pings)
                variable = beam_structure.create_blanking_interval(beam)
                #variable[:] = softChan.m_startSample*samplingInterval/1000000 #sample offset of raw data
                variable[:] = 2*(Depth[:,0]-(Depth[:,1]-Depth[:,0])/2)/sounder.m_soundVelocity
                variable = beam_structure.create_transducer_gain(beam) 
                variable[:] = beamGain
                variable = beam_structure.create_transmit_bandwidth(beam) 
                variable[:] = bandWidth
                variable = beam_structure.create_transmit_duration_nominal(beam) 
                variable[:] = pulseDuration/1000000
                variable = beam_structure.create_receive_duration_effective(beam) 
                variable[:] = pulseeffective
                variable = beam_structure.create_transmit_frequency_start(beam) 
                variable[:] = startFrequency
                variable = beam_structure.create_transmit_frequency_stop(beam) 
                variable[:] = stopFrequency
                variable = beam_structure.create_transmit_power(beam) 
                variable[:] = transmitPower
                variable = beam_structure.create_transmit_type(beam)
                variable[:] = pulseShape  
                variable = beam_structure.create_sound_speed_at_transducer(beam)
                variable[:] = sounder.m_soundVelocity         
                variable = beam_structure.create_platform_latitude(beam)
                variable[:] = Lat[:,0] #latitude of surface cell
                variable = beam_structure.create_platform_longitude(beam)
                variable[:] = Lon[:,0]  #longitude of surface cell
                variable = beam_structure.create_platform_heading(beam)
                variable = beam_structure.create_platform_pitch(beam)
                variable = beam_structure.create_platform_roll(beam)
                variable = beam_structure.create_platform_vertical_offset(beam)
                variable[:] = np.zeros(nb_pings)
                variable = beam_structure.create_tx_transducer_depth(beam)
                variable[:] = np.zeros(nb_pings)
        
        env_structure = xsf.EnvironmentGrp()
        env = env_structure.create_group(root)
        env_structure.create_dimension(env, {xsf.EnvironmentGrp.FREQUENCY_DIM_NAME: len(frequencyEnv)})
        
        variable = env_structure.create_frequency(env)
        variable[:] = frequencyEnv
        variable = env_structure.create_absorption_indicative(env)
        variable[:] = absorptionEnv
        variable = env_structure.create_sound_speed_indicative(env)
        variable[:] = sounder.m_soundVelocity
        
        platform_structure = xsf.PlatformGrp()
        platform = platform_structure.create_group(root)
        platform_structure.create_dimension(platform, {xsf.PlatformGrp.TRANSDUCER_DIM_NAME: nb_transduc,xsf.PlatformGrp.POSITION_DIM_NAME: 1,xsf.PlatformGrp.MRU_DIM_NAME: 1})

        variable = platform_structure.create_mru_offset_x(platform)
        variable[:] = 0
        variable = platform_structure.create_mru_offset_y(platform)
        variable[:] = 0
        variable = platform_structure.create_mru_offset_z(platform)
        variable[:] =0
        variable = platform_structure.create_mru_rotation_x(platform)
        variable[:] = 0
        variable = platform_structure.create_mru_rotation_y(platform)
        variable[:] = 0
        variable = platform_structure.create_mru_rotation_z(platform)
        variable[:] =0
        
        variable = platform_structure.create_position_offset_x(platform)
        variable[:] = 0
        variable = platform_structure.create_position_offset_y(platform)
        variable[:] = 0
        variable = platform_structure.create_position_offset_z(platform)
        variable[:] =0

        variable = platform_structure.create_transducer_offset_x(platform)
        variable[:] = trans_posx
        variable = platform_structure.create_transducer_offset_y(platform)
        variable[:] = trans_posy
        variable = platform_structure.create_transducer_offset_z(platform)
        variable[:] =trans_posz
        variable = platform_structure.create_transducer_rotation_x(platform)
        variable[:] = trans_orx
        variable = platform_structure.create_transducer_rotation_y(platform)
        variable[:] = trans_ory
        variable = platform_structure.create_transducer_rotation_z(platform)
        variable[:] =trans_orz
        
        variable = platform_structure.create_transducer_function(platform)
        variable[:] = 3*np.ones(nb_transduc)
        
    # os.remove(filename)
