# -*- coding: utf-8 -*-
"""
Created on Wed Feb  5 17:06:48 2020

@author: nlebouff
"""

# imports
import pymovies.pyMovies as mv

import numpy as np
import os
import sys
import datetime
import pytz

def hac_sounder_descr(FileName=None):
    """
        display sounders and transducers recorded in hac file
        
        returns: the sounders definition structure
        
        FileName: hac file 'FileName' is opened or reopened if specified, 
        otherwise current opened file is used
    """
    if FileName is not None:
        if  not os.path.isfile(FileName):
            print('file does not exist')
            sys.exit()
        
        mv.moOpenHac(FileName)
    
    # sounders presentation
    ###############################
    list_sounder=mv.moGetSounderDefinition()
    nb_snd=list_sounder.GetNbSounder()
    
    print('nb sondeurs = ' + str(nb_snd))
    for isdr in range(nb_snd):
        sounder = list_sounder.GetSounder(isdr)
        nb_transduc=sounder.m_numberOfTransducer
        print( 'sondeur ' + str(isdr) + ':    index: ' + str(sounder.m_SounderId) + '   nb trans:' + str(nb_transduc))
        for itr in range(nb_transduc):
            trans = sounder.GetTransducer(itr)      
            for ibeam in range(trans.m_numberOfSoftChannel):
                softChan = trans.getSoftChannelPolarX(ibeam)
                print( '   trans ' + str(itr) + ':    nom: ' + trans.m_transName + '   freq: ' + str(softChan.m_acousticFrequency/1000) + ' kHz')
    
    return list_sounder

     
def hac_goto(goto):
    """
        go to specified timestamp in hacfile and clear previous pings
        from memory (usefull if hac is re-written for exemple)
        
        goto: hac timestamp as POSIX timestamp datetime.datetime(yy, mm, dd,hh,mm,ss,tzinfo=datetime.timezone.utc).timestamp()
        or PingFan.m_meanTime.m_TimeCpu+PingFan.m_meanTime.m_TimeFraction/10000
    """
    ParameterDef=mv.moLoadReaderParameter()
    taille_chunk=ParameterDef.m_ChunckDef.m_chunkNumberOfPingFan
    
    
    ParameterDef.m_ChunckDef.m_chunkNumberOfPingFan=1
    mv.moSaveReaderParameter(ParameterDef)
    
        
    mv.moGoTo(goto)
    mv.moReadChunk() # read on more ping
    
    #clear memory from previous pings
    nb_pings=mv.moGetNumberOfPingFan()
    for ip in range(nb_pings,0,-1):
        MX= mv.moGetPingFan(ip-1)
        mv.moRemovePing(MX.m_computePingFan.m_pingId)
    
    
    ParameterDef.m_ChunckDef.m_chunkNumberOfPingFan=taille_chunk
    mv.moSaveReaderParameter(ParameterDef)
    
def hac_read_day_time(hactime):
    """
        return list of UTC datetime objects from hac timestamp list
        
        hactime: hac timestamp list
        as POSIX timestamp datetime.datetime(yy, mm, dd,hh,mm,ss,tzinfo=datetime.timezone.utc).timestamp()
        or PingFan.m_meanTime.m_TimeCpu+PingFan.m_meanTime.m_TimeFraction/10000
    """
    
    utc_tz=pytz.utc # define UTC timezone
    
    if np.size(hactime)==1:
        date=datetime.datetime.fromtimestamp(hactime,utc_tz)
    else:
        date =[]
        for i in range (len(hactime)):
            date.append(datetime.datetime.fromtimestamp(hactime[i],utc_tz))
    
    return date