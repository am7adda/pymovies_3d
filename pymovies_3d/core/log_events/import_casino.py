# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 14:00:44 2020

@author: Guillaume Brosse
"""




from pymovies.pyMovies import *



import os
import numpy as np
import pickle
import datetime
import time
#import plotly.express as px
import matplotlib.pyplot as plt
import matplotlib.dates as md
from matplotlib import colors
from sklearn.cluster import KMeans
import math
import numpy.matlib
import pandas as pd


####################################################################################################
#
# import_casino pour importer les données des fichiers casinos
# La fonction appelle le chemin ou est stocké le fichier Casino et renvoie :
#     - C (tableau de chaines de caractères): l'ensemble des données dans le fichier Casino
#     - Entete (tableau de chaines de caractères) : l'ensemble des entêtes de chaque colonnes
#     - index_events (int): les index de tous les évenements autres que ACQAUTO 
#     - date_heure : La date et l'heure au format np.datetime64
#
####################################################################################################


def import_casino (path_evt):
 
    #C = np.loadtxt(path_evt,dtype='U',delimiter = '\t' ,encoding='latin1')
    C0=pd.read_table(path_evt)
    C=C0.to_numpy(dtype='str')
    Entete =C[0,:]    
    C = C[1:(len(C)),:]
    index_events =  np.squeeze(np.where(C[:,6]!='ACQAUTO'))
    date_heure  = np.array((pd.to_datetime((np.char.add(C[:,0],C[:,1])), format="%d/%m/%Y%H:%M:%S")),dtype = np.datetime64)
  
    return C,Entete,index_events,date_heure;    



####################################################################################################
#
# Fonctions usuelles deux_de_suite_inferieur et deux_de_suite_superieur renvoyant repectivement 
# une liste en enlevant les indices lorsqu'ils sont à la suite (fonctions utiles pour 
# heures_dates_radiales_str )
#
####################################################################################################


    
def deux_de_suite_inferieur (liste):
    liste =liste.tolist()
    b_liste = []
    range_liste= range(len(liste))
    a_prec = liste[0]
    for i in range_liste : 
        a_courant = liste[i]
        if (a_prec==a_courant):
            b_liste.append(a_courant)
        elif(a_prec+1 < a_courant):
                b_liste.append(a_courant)
        a_prec = a_courant
    
    return b_liste             


def deux_de_suite_superieur (liste):
    liste =liste.tolist()
    b_liste = []
    range_liste= range(len(liste))
    a_prec = liste[len(liste)-1]
    for i in range_liste : 
        a_courant = liste[len(liste)-1-i]
        if (a_prec==a_courant):
            b_liste.append(a_courant)
        elif (a_prec-1 > a_courant):
                b_liste.append(a_courant)
        a_prec = a_courant
    b_liste.reverse()
    
    return b_liste    
    

####################################################################################################
#
# heures_dates_radiales_str est une fonction permettant de trouver les heures de début et de fin 
# de radiale dans unfifichier casino avec les codes DEBURAD,REPRAD,FINRAD,STOPRAD
# La fonction retourne quatre tableaux de chaines de caractères contenant respectivement les dates de 
# début et de fin ainsi que les heures de début et de fin de chaques radiales
#
####################################################################################################




def infos_radiales(C):
    index_heure  = np.squeeze(np.where(((C[:,10] == 'STOPRAD')| (C[:,10] == 'FINRAD')|(C[:,10] == 'REPRAD')| (C[:,10] == 'DEBURAD'))))
    C_hd = C[index_heure,:]
    
    
    index_heure_fin  = np.squeeze(np.where(((C_hd[:,10] == 'STOPRAD')| (C_hd[:,10] == 'FINRAD'))))
    index_heure_debut  = np.squeeze(np.where(((C_hd[:,10] == 'REPRAD')| (C_hd[:,10] == 'DEBURAD'))))
    index_heure_fin = deux_de_suite_inferieur(index_heure_fin)
    index_heure_debut = deux_de_suite_superieur(index_heure_debut)
    dates_debut = (C_hd[index_heure_debut,0]).tolist()
    dates_fin = (C_hd[index_heure_fin,0]).tolist()
    heure_debut = (C_hd[index_heure_debut,1]).tolist() 
    heure_fin  = (C_hd[index_heure_fin,1]).tolist() 
    
    
    if (len(dates_debut)>len(dates_fin)):
        dates_fin.append(C[(len(C)-1),0])
        heure_fin.append(C[(len(C)-1),1])
        index_heure_fin.append((len(C)-1))
        
    name_rad = (C_hd[index_heure_debut,11]).tolist() 
    vit_vent_vrai = []
    dir_vent_vrai = []
    for x in  range(len(index_heure_debut)):
        vit_str = (C_hd[index_heure_debut[x]:index_heure_fin[x],65]).tolist()
        dir_str =(C_hd[index_heure_debut[x]:index_heure_fin[x],66]).tolist()
        for i in range (len(vit_str)):
            vit_str[i] = float(vit_str[i].replace(',','.'))
            dir_str[i] = float(dir_str[i].replace(',','.'))
        vit_vent_vrai.append(np.mean(vit_str))
        dir_vent_vrai.append(np.mean(dir_str))
    # vit_vent_vrai = (C_hd[index_heure_debut,65]).tolist() 
    # dir_vent_vrai = (C_hd[index_heure_debut,66]).tolist() 
    for i in range (len(name_rad)):
        name_rad[i] = name_rad[i].replace(' ','')
        # vit_vent_vrai[i] = float(vit_vent_vrai[i].replace(',','.'))
        # dir_vent_vrai[i] = float(dir_vent_vrai[i].replace(',','.'))
                                          
    return dates_debut,dates_fin,heure_debut,heure_fin,name_rad,vit_vent_vrai,dir_vent_vrai
