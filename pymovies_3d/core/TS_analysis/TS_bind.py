﻿#!/usr/bin/python
# -*- coding: utf-8 -*-

import os

import pickle

def TS_bind(fpath,filename_ME70=None,filename_EK80=None,filename_EK80h=None):
    
    """
    Concatenate TS results (such as the ones created by samplecomputeTS, that are created by chunk) into one pickle file
    Different files are created for ME70, EK80 and horizontal EK80
       
       inputs:
               - fpath: path containing pickle files to concatenate
               - filename_XXX: filename (path + name) of resulting pickle file for echosounder XXX (ME70, EK80 or EK80h)
        output:
               - one pickle file is created for each specified echosounder, that can be loaded with:
               timeTarget,TSrange,TScomp,TSucomp,TSalong,TSathwart,TSposition,TSpositionGPS,TSlabel,TSfreq=pickle.load(open(filename_XXX,'rb'))
    """
    
    filelist = []
    for root, subFolders, files in os.walk(fpath):
        for file in files:
            if file.endswith('_TS.pickle'):
                filelist.append(os.path.join(root,file))
            
    timeTarget_all = []
    TSrange_all = []
    TScomp_all = []
    TSucomp_all = []
    TSalong_all = []
    TSathwart_all = []
    TSposition_all = []
    TSpositionGPS_all = []            
    TSlabel_all = []
    TSfreq_all = []

    if filename_EK80 is not None:
        for FileName in filelist:
            
            with open(FileName,'rb') as f:
               
                timeTarget,TSrange,TScomp,TSucomp,TSalong,TSathwart,TSposition,TSpositionGPS,TSlabel,TSfreq = pickle.load(f)
          
                timeTarget_all = timeTarget_all + timeTarget
                TSrange_all = TSrange_all + TSrange
                TScomp_all = TScomp_all + TScomp
                TSucomp_all = TSucomp_all + TSucomp
                TSalong_all = TSalong_all + TSalong
                TSathwart_all = TSathwart_all + TSathwart
                TSposition_all = TSposition_all + TSposition
                TSpositionGPS_all = TSpositionGPS_all + TSpositionGPS           
                TSlabel_all = TSlabel_all + TSlabel
                TSfreq_all = TSfreq_all + TSfreq
    
    
    
    with open(filename_EK80, 'wb') as f:
        pickle.dump([timeTarget_all,TSrange_all,TScomp_all,TSucomp_all,TSalong_all,TSathwart_all,TSposition_all,TSpositionGPS_all,TSlabel_all,TSfreq_all], f)

 