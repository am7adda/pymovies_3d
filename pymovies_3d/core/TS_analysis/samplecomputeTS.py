﻿# -*- coding: utf-8 -*-
"""
Created on Wed Nov 25 09:05:45 2020

@author: lberger

Effectue l'analyse des échos simples du fichier ou du répertoire indiqué, et l'enregistre en .pickle
   
   entrées:
           - chemin_ini: chemin du répertoire ou du fichier à traiter
           - chemin_config: chemin de la configuration M3D à utiliser (ESU, couches etc)
           - chemin_save: chemin du répertoire de sauvegarde des résultats
           - nameTransect: nom de sauvegarde (optionnel). Si None, les fichiers pickles seront
           nommés results_000X_EI.pickle
           - dateStart et dateEnd: dates de début et fin des séquences à écho-intégrer, au format 'dd/mm/yyyy' ou ['dd/mm/yyyy','dd/mm/yyyy'...]
           Si dateStart est None, tout le répertoire est écho-intégré
           - timeStart et timeEnd: heures de début et fin des séquences à écho-intégrer, au format 'hh:mm:ss' ou ['hh:mm:ss','hh:mm:ss'...]
    sortie:
           - Un fichier pickle par chunk lu est créé
"""

import pymovies.pyMovies as mv

import numpy as np

import calendar
import datetime
import time
import os
import sys
import pickle
import pymovies_3d.core.hac_util.hac_util as util

def sample_compute_TS(chemin_ini,chemin_config,chemin_save,indexSounder,indexTransducer,nameTransect=None,dateStart=None,timeStart=None,dateEnd=None,timeEnd=None):



    #load configuration
    if not os.path.exists(chemin_config):
        print('****** stop: chemin de configuration non valide')
        return
    
    mv.moLoadConfig(chemin_config)
    
    num_bloc=1
    
    if nameTransect is not None:
        str_Name='_'+nameTransect+'_'
    else:
        str_Name=''
    
    if dateStart is None:
        range_date = [-1]
        process_all=True
    else:
        if type(dateStart)==str:
            dateStart=[dateStart]
        if type(dateEnd)==str:
            dateEnd=[dateEnd]
        if type(timeStart)==str:
            timeStart=[timeStart]
        if type(timeEnd)==str:
            timeEnd=[timeEnd]
            
        range_date = range(len(dateStart))
        if (len(dateEnd)+len(timeStart)+len(timeEnd))/3!=len(dateStart):
            print('****** stop: les listes des dates/heures de début/fin n ont pas la même taille')
            return
        process_all=False
        
    for x in range_date :
        
        slice_end=False
        
        if not process_all:
    
            goto = calendar.timegm(time.strptime('%s %s' % ((dateStart[x]),(timeStart[x])), '%d/%m/%Y %H:%M:%S'))
            
            mv.moOpenHac(chemin_ini)
            
            util.hac_goto(goto)
            
            timeEndEI=calendar.timegm(time.strptime('%s %s' % (dateEnd[x],timeEnd[x]), '%d/%m/%Y %H:%M:%S'))
        else:
            # chargement de tous les fichiers HAC contenus dans le repertoire
            # et lecture du premier chunk
            mv.moOpenHac(chemin_ini)
            timeEndEI=calendar.timegm(datetime.datetime.now().timetuple())
            
        # presentation sondeurs
        #######################
        list_sounder=util.hac_sounder_descr()

        if  indexSounder>=list_sounder.GetNbSounder():
            print('index sondeur hors limite')
            sys.exit()
        sounder = list_sounder.GetSounder(indexSounder)
        if  indexTransducer>=sounder.m_numberOfTransducer:
            print('index transducteur hors limite')
            sys.exit()

        FileStatus = mv.moGetFileStatus()
        while not FileStatus.m_StreamClosed:
            
            timeTarget = []
            TSrange = []
            TScomp = []
            TSucomp = []
            TSalong = []
            TSathwart = []
            TSposition = []
            TSpositionGPS = []            
            TSlabel = []
            TSfreq = []
             
            mv.moReadChunk()
    
            nb_pings=mv.moGetNumberOfPingFan()
        
            for indexPing in range(nb_pings):
                MX= mv.moGetPingFan(indexPing)
                SounderDesc = MX.m_pSounder
                if SounderDesc.m_SounderId == sounder.m_SounderId :           
                    for beamIndex in range(SounderDesc.GetTransducer(indexTransducer).m_numberOfSoftChannel):
                        for index in range(len(MX.m_splitBeamData)):
                            if MX.m_splitBeamData[index].m_parentSTId==SounderDesc.GetTransducer(indexTransducer).getSoftChannelPolarX(beamIndex).m_softChannelId:
                                timeTarget.append(util.hac_read_day_time(MX.m_meanTime.m_TimeCpu+MX.m_meanTime.m_TimeFraction/10000))
                                if SounderDesc.GetTransducer(indexTransducer).m_pulseShape==2 :
                                    targets=MX.m_splitBeamData[index].m_targetListFM
                                else:
                                    targets=MX.m_splitBeamData[index].m_targetListCW
                                    
                                for indexTarget in range(len(targets)):           
                                    TSrange.append(targets[indexTarget].m_targetRange)
                                    TSalong.append(targets[indexTarget].m_AlongShipAngleRad)
                                    TSathwart.append(targets[indexTarget].m_AthwartShipAngleRad)
                                    TSlabel.append(targets[indexTarget].m_trackLabel)
                                    
                                    TSposition.append([])
                                    TSpositionGPS.append([])
                                    
                                    TSposition[-1].append([targets[indexTarget].m_positionWorldCoord.x,targets[indexTarget].m_positionWorldCoord.y,targets[indexTarget].m_positionWorldCoord.z])
                                    TSpositionGPS[-1].append([targets[indexTarget].m_positionGeoCoord.x,targets[indexTarget].m_positionGeoCoord.y,targets[indexTarget].m_positionGeoCoord.z])
                                    
                                    if SounderDesc.GetTransducer(indexTransducer).m_pulseShape==2:
                                        freqs= targets[indexTarget].m_freq
                                        tscomp=targets[indexTarget].m_compensatedTS
                                        tsucomp=targets[indexTarget].m_unCompensatedTS
                                        nbFreqs = len(freqs)
                                        freqsRanges = range(nbFreqs)
                                        TSfreq.append([])
                                        TScomp.append([])
                                        TSucomp.append([])
                                        TSfreq[-1].append([freqs[indexFreq] for indexFreq in freqsRanges])
                                        TScomp[-1].append([tscomp[indexFreq] for indexFreq in freqsRanges])
                                        TSucomp[-1].append([tsucomp[indexFreq] for indexFreq in freqsRanges])
                                    else:
                                        TSfreq.append(SounderDesc.GetTransducer(indexTransducer).getSoftChannelPolarX(beamIndex).m_acousticFrequency)
                                        TScomp.append(targets[indexTarget].m_compensatedTS)
                                        TSucomp.append(targets[indexTarget].m_unCompensatedTS)
                    

                
                
            if ((MX.m_meanTime.m_TimeCpu+MX.m_meanTime.m_TimeFraction/10000)>timeEndEI) & (not process_all):
                slice_end=True
                break
            
            
            # Save results
            if not os.path.exists(chemin_save):
                os.makedirs(chemin_save)
            
            str_bloc = "%04d" % (num_bloc)
    
            # à l'avenir on pourra choisir un format netcdf pour le stockage des résultats
            with open('%s/results_%s%s_TS.pickle' % (chemin_save, str_Name, str_bloc), 'wb') as f:
                pickle.dump([timeTarget,TSrange,TScomp,TSucomp,TSalong,TSathwart,TSposition,TSpositionGPS,TSlabel,TSfreq], f)
            
            #clear memory from previous pings
            for ip in range(nb_pings,0,-1):
                MX= mv.moGetPingFan(ip-1)
                mv.moRemovePing(MX.m_computePingFan.m_pingId)
        
            num_bloc=num_bloc+1
            
            if slice_end:
                break

    return
