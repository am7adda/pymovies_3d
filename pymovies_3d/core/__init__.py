# -*- coding: utf-8 -*-

"""Top-level package for core  Movies3D scripts"""

__author__ = """Laurent Berger"""
__email__ = "laurent.berger@ifremer.fr"
__version__ = "0.1.0"
