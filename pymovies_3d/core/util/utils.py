# -*- coding: utf-8 -*-
"""
Created on Fri Apr 17 16:31:04 2020

@author: Guillaume Brosse
"""

import numpy as np
import datetime

import pytz

      
            

def Sv_moy (Sv,Sv_surfEKxx_db,freqs_EKxx_db,indice,indice_decalage):
    Sv_moy_temp=np.zeros([len(Sv),len(Sv[indice]),len(freqs_EKxx_db[0][indice])])
    Sv_moy_temp[:,:,:]=np.nan
    for j in range (len(freqs_EKxx_db[0][indice])):
        Sv_moy_temp[:,:,j] = Sv[:,:,j+indice_decalage] 
    
    Sv_moy = np.zeros([len(Sv_moy_temp),len(Sv_moy_temp[1])])
    for i in range (len(Sv_moy_temp)):
    
        for j in range (len(Sv_moy_temp[0])): 
            Sv_moy[i,j] = 10*np.log10(np.nanmean(10**((Sv_moy_temp[i,j,:])/10)))
    Sv_moy[np.where(Sv_moy<=-100)] = -100      
    return Sv_moy

# conversion de date et heure en chaine de caract au format datetime

def convert_string_to_datetime (date_time_str):
 date_time_obj = []
 utc_tz=pytz.utc
 for i in range (len(date_time_str)):
     date_time_naive = datetime.datetime.strptime(date_time_str[i], '%d/%m/%Y%H:%M:%S')
     date_time_obj.append(date_time_naive.replace(tzinfo=utc_tz))
 
 return date_time_obj

