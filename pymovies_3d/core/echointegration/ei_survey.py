


from pymovies_3d.core.echointegration.batch_ei_multi_threshold import batch_ei_multi_threshold
from pymovies_3d.core.echointegration.sampleechointegration import sample_echo_integration
from pymovies_3d.core.echointegration.ei_bind import ei_bind

import os
import glob
import numpy as np

def ei_survey_transects(path_hac_survey,path_config,path_save,date_start,date_end,time_start,time_end,premiere_date,name_transects=None,ME70=False,EK80=True,EK80h=False)  :
   
    """
    Process EI over specified transects periods (potentially partitionned in different parts) and specified echosounders (ME70, EK80 and horizontal EK80)
       
       inputs:
               - path_hac_survey: path with hac files (they can be in subdirectories)
               - path_config: path of M3D configuration (with ESU definition, layers and EI threshold)
               - path_save: path for resulting EI files
               - date_start and date_end: lists of start and end dates of the transects to be echo-integrated, format ['dd/mm/yyyy',['dd/mm/yyyy','dd/mm/yyyy'],...]
               - time_start and time_end: lists of start and end times of the transects to be echo-integrated, format ['hh:mm:ss',['hh:mm:ss','hh:mm:ss'],...]
               - name_transects: list of names of transects (such as ['Line1','Line13',...])
                       If None, saved files names will be prefixed with ddmmyyy_Thhmmss(start)_to_ddmmyyy_Thhmmss(end)
               - ME70/EK80/EK80h: True if results are to be saved (just EK80 is activated as default value)
        output:
               - one EI pickle file per transects is created in path_save directory for each specified echosounder
               they can be loaded with:
               time_XXX,depth_surface_XXX,depth_bottom_XXX,Sv_XXX,Sv_XXX,Sa_surfXXX,Sa_botXXX,lat_surfXXX,lon_surfXXX,lat_botXXX,lon_botXXX,vol_surfXXX,freqs_XXX=pickle.load(open(filename,'rb'))
               
    """
    if type(date_start)==str:
            date_start=[date_start]
    if type(date_end)==str:
        date_end=[date_end]
    if type(time_start)==str:
        time_start=[time_start]
    if type(time_end)==str:
        time_end=[time_end]
   
    range_date = range(len(date_start))
    if (len(date_end)+len(time_start)+len(time_end))/3!=len(date_start):
       print('****** stop: length of time/date lists are inconsistent')
       return
   
    if not os.path.exists(path_save):
         os.makedirs(path_save)
         
    for x in range_date:
        
        if name_transects is None:
            if (np.size(date_start[x])!=1) | (np.size(date_end[x])!=1) | (np.size(time_start[x])!=1) | (np.size(time_end[x])!=1):
                print('****** stop: transects are multi-sequences. Specify names')
                return
            name_transect=date_start[x]+'_T'+time_start[x]+'_to_'+date_end[x]+'_T'+time_end[x]
            name_transect=name_transect.replace('/','')
            name_transect=name_transect.replace(':','')
        else:
            # name_transect=name_transects[x]
            name_transect=name_transects[x+premiere_date]
            
        sample_echo_integration(path_hac_survey,path_config,path_save+"/"+name_transect+"/",nameTransect=name_transect,dateStart=date_start[x],timeStart=time_start[x],dateEnd=date_end[x],timeEnd=time_end[x])
        
        fname_ME70=None
        fname_EK80=None
        fname_EK80h=None
        if ME70:
            fname_ME70=path_save+ "/" + name_transect + "_ME70" + ".pickle"
        if EK80:
            fname_EK80=path_save+ "/" + name_transect + "_EK80" + ".pickle"
        if EK80h:
            fname_EK80h=path_save+ "/" + name_transect + "_EK80h" + ".pickle"
       
        ei_bind(path_save+"/"+name_transect+"/",filename_ME70=fname_ME70,filename_EK80=fname_EK80,filename_EK80h=fname_EK80h)       

def ei_survey_RUN(path_hac_survey,path_config,path_save,runs=None,ME70=False,EK80=True,EK80h=False)  :
   
    """
    Process EI over specified RUN directories and specified echosounders (ME70, EK80 and horizontal EK80)
       
       inputs:
               - path_hac_survey: path of RUN directories
               - path_config: path of M3D configuration (with ESU definition, layers and EI threshold)
               - path_save: path for resulting EI files
               - runs: list of RUNs to process (such as ['RUN001','RUN003'])
                       If None, all RUNs directories in path_hac_survey are processed
               - ME70/EK80/EK80h: True if results are to be saved (just EK80 is activated as default value)
        output:
               - one EI pickle file per run is created in path_save directory for each specified echosounder
               they can be loaded with:
               time_XXX,depth_surface_XXX,depth_bottom_XXX,Sv_XXX,Sv_XXX,Sa_surfXXX,Sa_botXXX,lat_surfXXX,lon_surfXXX,lat_botXXX,lon_botXXX,vol_surfXXX,freqs_XXX=pickle.load(open(filename,'rb'))
               
    """
    
    if runs is None:  #lists all runs
        runs = glob.glob1(path_hac_survey,"RUN*")
    
    for run in runs:
        
        chemin_ini=path_hac_survey + "/" + run + "/"
        chemin_save=path_save+ "/" + run + "/"
        if not os.path.exists(chemin_save):
                os.makedirs(chemin_save)
        
        sample_echo_integration(chemin_ini,path_config,chemin_save,nameTransect=run)
        
        fname_ME70=None
        fname_EK80=None
        fname_EK80h=None
        if ME70:
            fname_ME70=path_save+ "/" + run + "_ME70" + ".pickle"
        if EK80:
            fname_EK80=path_save+ "/" + run + "_EK80" + ".pickle"
        if EK80h:
            fname_EK80h=path_save+ "/" + run + "_EK80h" + ".pickle"
       
        ei_bind(path_save,filename_ME70=fname_ME70,filename_EK80=fname_EK80,filename_EK80h=fname_EK80h)
        

def ei_hacfile(path_hac_survey,hacfilename,path_config,path_save,ME70=False,EK80=True,EK80h=False)  :
   
    """
    Process EI over specified hac file and specified echosounders (ME70, EK80 and horizontal EK80)
       
       inputs:
               - path_hac_survey: hac directory
               - hacfilename: name of hac file
               - path_config: path of M3D configuration (with ESU definition, layers and EI threshold)
               - path_save: path for resulting EI file
               - ME70/EK80/EK80h: True if results are to be saved (just EK80 is activated as default value)
        output:
               - one EI pickle file is created in path_save directory for each specified echosounder
               they can be loaded with:
               time_XXX,depth_surface_XXX,depth_bottom_XXX,Sv_XXX,Sv_XXX,Sa_surfXXX,Sa_botXXX,lat_surfXXX,lon_surfXXX,lat_botXXX,lon_botXXX,vol_surfXXX,freqs_XXX=pickle.load(open(filename,'rb'))
               
    """

    file_ini=path_hac_survey + "/" + hacfilename
    chemin_save=path_save + "/" + hacfilename[:-4]+ "/"
    if not os.path.exists(chemin_save):
            os.makedirs(chemin_save)
    
    sample_echo_integration(file_ini,path_config,chemin_save)
    
    fname_ME70=None
    fname_EK80=None
    fname_EK80h=None
    if ME70:
        fname_ME70=path_save+ "/" + hacfilename[:-4] + "_ME70" + ".pickle"
    if EK80:
        fname_EK80=path_save+ "/" + hacfilename[:-4] + "_EK80" + ".pickle"
    if EK80h:
        fname_EK80h=path_save+ "/" + hacfilename[:-4] + "_EK80h" + ".pickle"
   
    ei_bind(path_save,filename_ME70=fname_ME70,filename_EK80=fname_EK80,filename_EK80h=fname_EK80h)
        
               
        
def ei_survey_multi_threshold (survey_name,path_hac_survey,path_config,path_save,thresholds,datedebut,datefin,heuredebut,heurefin,runs,filename_ME70,filename_EK80,filename_EK80h)  :
    #batch_ei_multi_threshold(path_hac_survey,path_config,path_save,thresholds,runs)
    # batch_ei_multi_threshold(path_hac_survey,path_config,path_save,thresholds,runs,'Fish',dateStart=datedebut,timeStart=heuredebut,dateEnd=datefin,timeEnd=heurefin)
    batch_ei_multi_threshold(path_hac_survey,path_config,path_save,thresholds,nameTransect = 'Fish',dateStart=datedebut,timeStart=heuredebut,dateEnd=datefin,timeEnd=heurefin)

        
    if runs is not None:    
        for ir in runs:
            for t in thresholds:
                str_run = "%03d" % (ir)
                path_results= '%s/RUN%s/%d/' % (path_save,str_run,t)
                ei_bind(path_results,filename_ME70,filename_EK80,filename_EK80h)
                
    
    else : 
        for t in thresholds:
            path_results= '%s/%d/' % (path_save,t)
            ei_bind(path_results,filename_ME70,filename_EK80,filename_EK80h)
