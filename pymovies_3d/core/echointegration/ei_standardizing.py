# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 14:38:54 2020

@author: Guillaume Brosse
"""


"""

entrees : -freqs_XXxx_db,Sv_surfXXxx_db,Sa_surfXXxx_db,depth_surface_XXxx_db,depth_bottom_XXxx_db,
          lat_surfXXxx_db,lon_surfXXxx_db,time_XXxx_db ---> Données brutes
          
          -indices_transduc_choisis ---> indices des transducteurs à étudier
          
          -date_time_debut,date_time_fin ---> tableaux des heures de début et de fin de radiales 
          
          -save = False ---> Par defaut : False. Si True alors les résultats seronts sauvegardés dans 
                             des fichiers .pickle
          
          -filename=None ---> Par defaut : False. Si True alors le fichier .pickle aura un nom défini

Sorties : Si save = False : Les données suivantes sont retournées à la fin de la fonction

                            -freq_MFR,Sv,Sa,Depth,Lat,Lon,time_XXxx_db ---> données d'echo-intégration remaniées 
                            
                            -nb_transduc_choisis ---> nombre de transducteurs étudiés
                            
                            -indices_radiales ---> indices de début et de fin de chaque radiales
                            
                            -freqs_moy_transducteur, Sv_moy_transducteur_x ---> données moyennes par transducteurs pour affichage
          
            
          Si save = true : Les données freq_MFR,freqs_moy_transducteur,Sv,Sv_moy_transducteur_x,Sa,Depth,Lat,Lon,
                           time_XXxx_db,nb_transduc_choisis,indices_radiales seront sauvegardées dans un fichier .pickle

"""

from pymovies_3d.core.echointegration.batch_ei_multi_threshold import batch_ei_multi_threshold
from pymovies_3d.core.echointegration.ei_bind import ei_bind
import numpy as np
import pymovies_3d.core.util.utils as ut
import pymovies_3d.core.hac_util.hac_util as hac_util

import pickle


def scaling (freqs_XXxx_db,Sv_surfXXxx_db,Sa_surfXXxx_db,depth_surface_XXxx_db,depth_bottom_XXxx_db,
             lat_surfXXxx_db,lon_surfXXxx_db,time_XXxx_db, 
             indices_transduc_choisis,date_time_debut,date_time_fin,premiere_date,name_radiales,save = False, filename=None):
    

    
    nb_transduc = len(freqs_XXxx_db[0])

    # Vérification : l'indice ne doit pas dépasser le nombre de sondeurs total sinon il est supprimé 
    indices_transduc_choisis = np.delete(indices_transduc_choisis,[np.where(indices_transduc_choisis>=nb_transduc)])

    
    nb_transduc_choisis = len(indices_transduc_choisis)
    
    freq_prec = freqs_XXxx_db[0][indices_transduc_choisis[0]]
    liste_indice_max = []
    for x in range(nb_transduc_choisis) :
        liste = []
        for y in range (len(freqs_XXxx_db)):
            liste.append(len(freqs_XXxx_db[y][indices_transduc_choisis[x]]))   
        print(min(liste))
        liste_indice_max.append(min(liste))
    print(liste_indice_max)
    if (len(liste_indice_max)!=nb_transduc_choisis):
        print("ERREUR nombre de transducteurs")
    
    for x in range(nb_transduc_choisis-1) : 
  
        freq_MFR=np.concatenate((freq_prec,freqs_XXxx_db[0][indices_transduc_choisis[x+1]][:liste_indice_max[x+1]]))
        freq_prec = freq_MFR
        
    Depth=np.squeeze(depth_surface_XXxx_db[:][0][:])
    # detection du fond : indice_fond représente l'indice de fond 
    detec_fond = max(max(max(depth_bottom_XXxx_db)))
    depth_surf_max=max(Depth[0])
    if depth_surf_max>detec_fond:
        indice_fond   = min( np.where(Depth[0] >=detec_fond)[0])
    else:
        indice_fond   =   len(Depth[0])
        
    Sv=np.zeros([len(Sv_surfXXxx_db),indice_fond,len(freq_MFR)])
    Lat=np.zeros([len(Sv_surfXXxx_db),len(Sv_surfXXxx_db[1][2])])
    Lon=np.zeros([len(Sv_surfXXxx_db),len(Sv_surfXXxx_db[1][2])])
    Sa=np.zeros([len(Sa_surfXXxx_db),indice_fond,len(freq_MFR)])
    Sa[:,:,:] = np.NaN
    
    if indice_fond >= len(Sv_surfXXxx_db[1][1]): 
        indice_fond = len(Sv_surfXXxx_db[1][1])
    
    indice_decalage = [0]
    
    for i in range(len(Sv_surfXXxx_db)):
        for j in range(indice_fond):
            Sv_prec = Sv_surfXXxx_db[i][indices_transduc_choisis[0]][j][:liste_indice_max[0]]
            Sa_prec = Sa_surfXXxx_db[i][indices_transduc_choisis[0]][j][:liste_indice_max[0]]
            
            # Depth[i,j]=np.squeeze(depth_surface_EK80_db[i][0][j])
            for x in range(nb_transduc_choisis-1) :
                Sv_temp=np.concatenate((Sv_prec,Sv_surfXXxx_db[i][indices_transduc_choisis[x+1]][j][:liste_indice_max[x+1]]))
                Sa_temp  =np.concatenate((Sa_prec,Sa_surfXXxx_db[i][indices_transduc_choisis[x+1]][j][:liste_indice_max[x+1]]))
                Sv_prec =Sv_temp
                Sa_prec =Sa_temp
                indice_decalage.append(max(indice_decalage)+len(Sv_surfXXxx_db[0][0][indices_transduc_choisis[x]]))
            Sv[i,j,:]= Sv_temp
            Sa[i,j,:] = Sa_temp
            
            #on prend la position du premier transducteur il faudrait faire la moyenne sur ceux choisis
            Lat[i,j]=np.array(lat_surfXXxx_db[i][indices_transduc_choisis[0]][j])
            Lon[i,j]=np.array(lon_surfXXxx_db[i][indices_transduc_choisis[0]][j])
        
        
    Depth=Depth[:,:indice_fond]
    
    Sv_moy_transducteur_x = []
    freqs_moy_transducteur = []
    
    for x in range(nb_transduc_choisis) :
        
        Sv_moy_transducteur_x.append(np.transpose(ut.Sv_moy (Sv,Sv_surfXXxx_db,freqs_XXxx_db,indices_transduc_choisis[x],indice_decalage[x])))
        freqs_moy_transducteur.append((np.median(freqs_XXxx_db[0][indices_transduc_choisis[x]]))/1000)
    
    
    
    name = np.array(name_radiales)
    nameliste = list(set(name))
    nameliste.sort()
    liste_nomrad = []
    liste_numrad = []
        

    
    datetime_finrad  = ut.convert_string_to_datetime(date_time_fin)
    datetime_debutrad  = ut.convert_string_to_datetime(date_time_debut)
    indices_debut_rad=[]
    indices_fin_rad=[]
    Time = hac_util.hac_read_day_time(time_XXxx_db)
    indice = []
    for i in range (len(datetime_debutrad)):
        # indice = []
        for j in range (len(Time)):
            
            if ((Time[j]<=datetime_finrad[i])and(Time[j]>=datetime_debutrad[i])) :
                 indice.append(j)
                 break
                 
        if len(indice)>0:
            # indices_debut_rad.append( min( indice))
            # indices_fin_rad.append(max( indice))
            # liste_nomrad.append(nameliste[i])
            # liste_numrad.append(i+1)
            liste_nomrad.append(nameliste[i+premiere_date])
            liste_numrad.append(i+premiere_date)
    
        
    # indices_radiales = np.zeros((2,len(indices_fin_rad)),dtype=int)
    # indices_radiales[0,:]=np.array(indices_debut_rad,dtype=int)
    # indices_radiales[1,:]=np.array(indices_fin_rad,dtype=int)
    Sv_moy_transducteur_x = np.array(Sv_moy_transducteur_x)
    

    
    print(len(indices_debut_rad))
    print(len(liste_nomrad))
 
    
    tableau_radiales = np.zeros((len(Sv)))
    
    for x in range (1,len(liste_nomrad)):
        tableau_radiales[indice[x-1]:indice[x]]=liste_numrad[x-1]
        
    tableau_radiales[indice[-1]:]=liste_numrad[len(liste_nomrad)-1]  
    
    # for x in range (len(liste_nomrad )):
    #     tableau_radiales[indices_debut_rad[x]:indices_fin_rad[x]]=x    

        
    nom_numero_radiales = (liste_nomrad,liste_numrad)
    if save :
        if filename is None :

            filename = "saved_results.pickle" 
        with open(filename, 'wb') as f:
            pickle.dump([freq_MFR,freqs_moy_transducteur,Sv,Sv_moy_transducteur_x,Sa,Depth,Lat,Lon,
                         Time,nb_transduc_choisis,tableau_radiales,nom_numero_radiales], f)
  
    else : 
        return freq_MFR,freqs_moy_transducteur,Sv,Sv_moy_transducteur_x,Sa,Depth,Lat,Lon,Time,nb_transduc_choisis,tableau_radiales,nom_numero_radiales