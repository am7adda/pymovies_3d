# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 15:56:08 2020

@author: Guillaume Brosse
"""


"""

Organisation de classif_multifreq.py 


|- import de librairies 
|
|- Ouverture d'un fichier Casino
|
|- Initialisation des variables, des chemins d'accès aux données et des 
|  préférences de visualisation
|
|- Echo-intégration et enregistrement des données brutes
|
|- Remaniement des données : données utiles à la classification 
|  mises sous forme de tableau dans les dimensions adéquates 
|                  |
|                  |-- enregistrement de Sv total, Sv moyen par transducteurs, Sa, de l'heure, 
|                      la profondeur, la latitude, la longitude, 
|                      la frequence, la frequence moyenne de chaque transducteur
|
|- Classification avec Kmeans 
|                  |
|                  |-- enregistrement de Sv total, Sv moyen par transducteurs, Sa, de l'heure, 
|                      la profondeur, la latitude, la longitude, 
|                      la frequence, la frequence moyenne de chaque transducteur,
|                      kmeans, labels, la réponse médiane par clusters
|
|- Selection des fréquences à afficher pour le Sv
|
|- Affichage du Sv moyen par transducteurs
|
|- Affichage des Sv en fonction de la plage de fréquences choisie 
|
|- Affichage des résultats de classification avec la réponse médiane en fréquence
|
|- Import des données Lat, Lon, Sa et Sa_log en CSV pour pouvoir visualiser (dans Qgis par exemple)
|  la densité de poissons dans un repère géogrgaphique.
|
|- Affichage de la densité de poisson sur une carte dans une figure 
|
|- Création de fichiers html pour visualiser la densité de poissons sur une carte
|  OpenStreetMap
|
|- Génération d'un rapport sur l'analyse de données (format .adoc)


"""

import pytz

from pymovies_3d.core.echointegration.batch_ei_multi_threshold import batch_ei_multi_threshold
from pymovies_3d.core.echointegration.ei_bind import ei_bind

import os
import numpy as np
import pickle
import datetime
import time
#import plotly.express as px
import matplotlib.pyplot as plt
import matplotlib.dates as md
from matplotlib import colors
# from sklearn.cluster import KMeans
import math
import numpy.matlib
import pymovies_3d.visualization.drawing as dr
import pymovies_3d.core.log_events.import_casino as ic
# import import_casino as ic 

import csv
import glob2
import pymovies_3d.core.hac_util.hac_util as au

import pymovies_3d.core.echointegration.ei_survey as ei
import pymovies_3d.core.echointegration.ei_bind as ei_bind
import pymovies_3d.core.echointegration.ei_standardizing as standardizing
import pymovies_3d.core.echointegration.ei_classification as classif
import pymovies_3d.core.util.utils as ut
import pymovies_3d.visualization.map_drawing as md

# import pymovies_3d.core.export.export_sonarNetCdf as export
####################################################################################################
#
# Initialisation des variables : A modifier selon les besoins 
#
####################################################################################################

# path_evt = '/echosonde/data/PELGAS2019/Casino/casino_pelgas2019.csv'
path_evt = 'D:\PHOENIX18\Casino\PHOENIX2018_casino_Copie.txt'
#path_evt= 'F:\EchoSonde\EchoSonde2019-2\CasinoManuelEchoSonde2019-2.txt'
# C,Entete,index_events,date_heure = ic.import_casino_bio(path_evt)
C,Entete,index_events,date_heure = ic.import_casino(path_evt)
datedebut,datefin,heuredebut,heurefin,nameTransect, vit_vent_vrai,dir_vent_vrai  = ic.infos_radiales(C)
# datedebut,datefin,heuredebut,heurefin,nameTransect, vit_vent_vrai,dir_vent_vrai  = ic.infos_radiales_echosonde(C)

#supression des variables qui ne sont plus utiles
del C,Entete,index_events,date_heure

indsuppr=[]
for i in range (len(vit_vent_vrai) ):
    if (vit_vent_vrai[i]>=25.0):
        indsuppr.append(i)
        
nbsuppr=0
for i in range (len(indsuppr) ):
    nbsuppr=nbsuppr+1
    del datedebut[indsuppr[i]-nbsuppr],datefin[indsuppr[i]-nbsuppr],heuredebut[indsuppr[i]-nbsuppr],heurefin[indsuppr[i]-nbsuppr],nameTransect[indsuppr[i]-nbsuppr]


premiere_date=0
derniere_date=1

# # essais
datedebut = datedebut[premiere_date:derniere_date]
heuredebut = heuredebut[premiere_date:derniere_date]
datefin=datefin[premiere_date:derniere_date]
heurefin=heurefin[premiere_date:derniere_date]

#print(premiere_date)

date_time_debut= []
date_time_fin= []
for i in range (len(datedebut)):
    date_time_debut.append("%s%s" %(datedebut[i],heuredebut[i]))
    date_time_fin.append("%s%s" %(datefin[i],heurefin[i]))


#path windows
path_hac_survey = 'F:\HERMES'
path_config = 'F:/Configs_M3D/configsAnalyseM3D/EIlay'
survey_name='PHOENIX18'
# path_hac_survey = 'D:\SBES_MBES\ECHOSONDE2019-2\RUN370'
# path_config = 'F:/Configs_M3D/configsAnalyseM3D/EIlay'
# survey_name='Echosonde2019_2'
# datedebut='05/06/2018'
# datefin='05/06/2018'

#path linux
# path_hac_survey = '/echosonde/data/PELGAS2019/HERMES'
# path_config = '/echosonde/data/PELGAS2019/Config_M3D/ConfigsAnalyseM3D/EIlay'
# survey_name='PELGAS19'
# # datedebut='05/06/2018'
# # datefin='05/06/2018'

path_save = 'D:\PHOENIX18_2emeEI\PHOENIX18_ACOU3_calib' + '/Result/' + survey_name +'/'
# path_save = 'D:\Echosonde2019_2_Rad4' + '/Result/' + survey_name +'/'
# path_save = path_hac_survey + '/Result/' + survey_name +'/'

# heuredebut ='13:44:00'
# heurefin ='14:14:00'

# Variable permettant d'activer l'utilisation de données enregistées dans un fichier pickle
save_data = True

# # Variable permettant la modification de la classification 
# modif_clustering = True
# # Variable permettant la réorganisation des variables d'echo-integration 
# reorganisation_EI = True
#Si les deux variables précédentes sont False alors on charge par defaut le fichier filename_clustering s'il à été enregistré auparavant
reorganisation_EI = True
modif_clustering = True
thresholds=[-80]

path_config=path_config + '/' + str(thresholds[0])

#tic=time.clock()


path_csv= path_save + 'csv/'
path_png= path_save + 'png/'
path_html= path_save + 'html/'
path_netcdf= path_save + 'netcdf/'
#If save path does not exist, create it
if not os.path.exists(path_save):
    os.makedirs(path_save)

if not os.path.exists(path_csv):
    os.makedirs(path_csv)

if not os.path.exists(path_png):
    os.makedirs(path_png)

if not os.path.exists(path_html):
    os.makedirs(path_html)
    
if not os.path.exists(path_netcdf):
    os.makedirs(path_netcdf)

filename_ME70='%s/%s-TH%d-ME70-EIlay.pickle' % (path_save,survey_name,thresholds[0])
filename_EK80='%s/%s-TH%d-EK80-EIlay.pickle' % (path_save,survey_name,thresholds[0])
filename_EK80h='%s/%s-TH%d-EK80h-EIlay.pickle' % (path_save,survey_name,thresholds[0])
    

if save_data :

    filename_data_ME70 = "%s/saved_data_for_ME70_all.pickle"  % (path_save)
    filename_data_EK80 = "%s/saved_data_for_EK80_all.pickle"   % (path_save)
    filename_data_EK80h = "%s/saved_data_for_EK80h_all.pickle"  % (path_save)
    filename_clustering_ME70 = "%s/saved_clustering_for_ME70_all.pickle"  % (path_save)
    filename_clustering_EK80 = "%s/saved_clustering_for_EK80_all.pickle" % (path_save)
    filename_clustering_EK80h = "%s/saved_clustering_for_EK80h_all.pickle" % (path_save)




####################################################################################################
#
# Echo-integration
#
####################################################################################################



var_choix = input('voulez vous modifier les données de l EI ?  \n   y or n \n')
if var_choix == 'y' :
    ei.ei_survey_transects(path_hac_survey,path_config,path_save,datedebut,datefin,heuredebut,heurefin,premiere_date,nameTransect,False,True,False)

#concaténation de tous des résultats EI EK80 de tous les transects pour analyse MFR à l'échelle de la campagne
ei_bind.ei_bind_transects(path_save,None,filename_EK80,None)

# ####################################################################################################
#
# Remaniement des données et classification 
#
####################################################################################################


# Choisir les indices des transducteurs souhaités (a renseigner dans l'ordre croissant)
indices_transduc_choisis = np.array([0,1,2,3,4,5])

# nb_transduc_choisis =  len(indices_transduc_choisis)


if save_data :
    
# ré-organistion des données d'écho-intégration mise au format standard     
    
    if (var_choix =='y' or reorganisation_EI) :
        
                
        # Commenter/Decommenter pour choisir quels fichiers charger 
        print('chargement des donnees...')
        with open(filename_EK80,'rb') as f:
            time_EK80_db,depth_surface_EK80_db,depth_bottom_EK80_db,Sv_surfEK80_db,Sv_botEK80_db,Sa_surfEK80_db,Sa_botEK80_db,lat_surfEK80_db,lon_surfEK80_db,lat_botEK80_db,lon_botEK80_db,vol_surfEK80_db,freqs_EK80_db = pickle.load(f)
        
        #with open(filename_EK80h) as f:
        #    time_EK80h_db,depth_surface_EK80h_db,Sv_surfEK80h_db,Sa_surfEK80h_db,lat_surfEK80h_db,lon_surfEK80h_db,vol_surfEK80h_db,freqs_EK80_db = pickle.load(f)
        
        #with open(filename_ME70) as f:
        #    time_ME70_db,depth_surface_ME70_db,depth_bottom_ME70_db,Sv_surfME70_db,Sv_botME70_db,Sa_surfME70_db,Sa_botME70_db,lat_surfME70_db,lon_surfME70_db,lat_botME70_db,lon_botME70_db,vol_surfME70_db,freqs_ME70_db = pickle.load(f)

    
        
        standardizing.scaling(freqs_EK80_db,Sv_surfEK80_db,Sa_surfEK80_db,depth_surface_EK80_db,depth_bottom_EK80_db,lat_surfEK80_db,
                              lon_surfEK80_db,time_EK80_db,indices_transduc_choisis,date_time_debut,date_time_fin,premiere_date,nameTransect,save=save_data, filename=filename_data_EK80)
        
       
        
        del time_EK80_db,depth_surface_EK80_db,depth_bottom_EK80_db,Sv_surfEK80_db,Sv_botEK80_db,Sa_surfEK80_db,Sa_botEK80_db,lat_surfEK80_db,lon_surfEK80_db,lat_botEK80_db,lon_botEK80_db,vol_surfEK80_db,freqs_EK80_db
        
        # with open(filename_data_EK80,'rb') as f:
        #     freq_MFR,Sv,Sa,Depth,Lat,Lon,Time = pickle.load(f)

# Classification des données d'écho-intégration avec classif.clustering

    if (var_choix =='y' or modif_clustering) :
        
        with open(filename_data_EK80,'rb') as f:
            freq_MFR,freqs_moy_transducteur,Sv,Sv_moy_transducteur_x,Sa,Depth,Lat,Lon,Time,nb_transduc,tableau_radiales,nom_numero_radiales = pickle.load(f)
        nombre_de_categories = (int) (input('Combien de clusters voulez-vous ?  \n'))
        classif.clustering(Sv,Sv_moy_transducteur_x,freq_MFR,freqs_moy_transducteur,nombre_de_categories,Sa,Time,Depth,Lat,Lon,
                           nb_transduc,tableau_radiales,nom_numero_radiales,save =save_data,filename = filename_clustering_EK80)
        
        
        del freq_MFR,freqs_moy_transducteur,Sv,Sv_moy_transducteur_x,Sa,Depth,Lat,Lon,Time,nb_transduc,tableau_radiales,nom_numero_radiales
        
        
        with open(filename_clustering_EK80,'rb') as f:
            kmeans,labels,Q1_,Q3_,med,nombre_de_categories,Sv,Sv_moy_transducteur_x,Sa,Time,freq_MFR,freqs_moy_transducteur,Depth,Lat,Lon,nb_transduc,tableau_radiales,nom_numero_radiales = pickle.load(f)
    else :
        # with open(filename_data_EK80,'rb') as f:
        #     freq_MFR,Sv,Sa,Depth,Lat,Lon = pickle.load(f)
        
        
        # Ouverture du fichier pickle contenat la classification et l'Echo-intégration 
        
        with open(filename_clustering_EK80,'rb') as f:
            kmeans,labels,Q1_,Q3_,med,nombre_de_categories,Sv,Sv_moy_transducteur_x,Sa,Time,freq_MFR,freqs_moy_transducteur,Depth,Lat,Lon,nb_transduc,tableau_radiales,nom_numero_radiales = pickle.load(f)


# Si vous ne sopuhaitez pas sauvegarder les données de la classification

else :
     # Commenter/Decommenter pour choisir quels fichiers charger 
    print('chargement des donnees...')
    with open(filename_EK80,'rb') as f:
            time_EK80_db,depth_surface_EK80_db,depth_bottom_EK80_db,Sv_surfEK80_db,Sv_botEK80_db,Sa_surfEK80_db,Sa_botEK80_db,lat_surfEK80_db,lon_surfEK80_db,lat_botEK80_db,lon_botEK80_db,vol_surfEK80_db,freqs_EK80_db = pickle.load(f)
        
    #with open(filename_EK80h) as f:
        #    time_EK80h_db,depth_surface_EK80h_db,Sv_surfEK80h_db,Sa_surfEK80h_db,lat_surfEK80h_db,lon_surfEK80h_db,vol_surfEK80h_db,freqs_EK80_db = pickle.load(f)
        
    #with open(filename_ME70) as f:
        #    time_ME70_db,depth_surface_ME70_db,depth_bottom_ME70_db,Sv_surfME70_db,Sv_botME70_db,Sa_surfME70_db,Sa_botME70_db,lat_surfME70_db,lon_surfME70_db,lat_botME70_db,lon_botME70_db,vol_surfME70_db,freqs_ME70_db = pickle.load(f)


    freq_MFR,freqs_moy_transducteur,Sv,Sv_moy_transducteur_x,Sa,Depth,Lat,Lon,Time,nb_transduc_choisis,tableau_radiales,nom_numero_radiales =  standardizing.scaling(freqs_EK80_db,Sv_surfEK80_db,Sa_surfEK80_db,depth_surface_EK80_db,depth_bottom_EK80_db,lat_surfEK80_db,lon_surfEK80_db,
                                                                                                                                                 time_EK80_db,indices_transduc_choisis,date_time_debut,date_time_fin,premiere_date,nameTransect,save=save_data)
    nombre_de_categories = (int) (input('Combien de clusters voulez-vous ?  \n'))
    kmeans,labels,Q1_,Q3_,med= classif.clustering(Sv,Sv_moy_transducteur_x,freq_MFR,freqs_moy_transducteur,nombre_de_categories,Sa,Time,Depth,Lat,Lon,nb_transduc,tableau_radiales,nom_numero_radiales)


    
    


####################################################################################################
#
# A mofifier : 
#   nbfreqs : le nombre de fréquences voulues dans l'intervalle linéaire ou log
#   space_custom_temp : a modifier si on veut regarder l'echo à des fréquences particulières
#   freq_voulues : lui donner les valeurs des intervales de fréquence recherchés 
#                 (soit space_log, space_lin ou space_custom)
#
####################################################################################################
    
nbfreqs = 6 #nombre de frequences à afficher lors de l'affichages des Sv/ESU

# space_custom_temp =[70000,200000]
# space_custom_temp =[18000,38000,70000,120000,200000,333000]
freq_voulues = []
space_log=[]
space_lin=[]
space_log_temp =  np.logspace(math.log10(freq_MFR[1]),math.log10(freq_MFR[len(freq_MFR)-1]),nbfreqs) 
space_lin_temp =  np.linspace(freq_MFR[1],(freq_MFR[len(freq_MFR)-1]),nbfreqs) 
space_custom_temp =[18000,38000,70000,120000,200000,333000]

space_custom = []
for i in range (len(space_custom_temp)) :
        space_custom.append(min(freq_MFR, key=lambda x:abs(x-space_custom_temp[i])))
for i in range (len(space_log_temp)) :
        space_log.append(min(freq_MFR, key=lambda x:abs(x-space_log_temp[i])))

for i in range(len(space_lin_temp)) :
        space_lin.append(min(freq_MFR, key=lambda x:abs(x-space_lin_temp[i])))
        
freq_voulues = space_custom #A modifier ici


          
#########################################################################################################
#                  
# Affichage du Sv moyen par transducteurs
# Affichage de la classification et de la réponse fréquentielle par clusters
# Affichage des Sv/ESU dans l'intervale définit précédemment 
#
#########################################################################################################

# Affichage du Sv moyen par transducteurs

# for x in range (len(nom_numero_radiales[1])):
#     fig, figue  = plt.subplots(figsize=(18,12),dpi=80)
#     indices_rad = np.squeeze(np.where(tableau_radiales==nom_numero_radiales[1][x]))
#     dr.DrawMultiEchoGram(Sv_moy_transducteur_x[:,:,indices_rad], Depth,nb_transduc,freqs = freqs_moy_transducteur,title='Sv moyen sondeur %d kHz')    
#     fig.savefig("%s/figure_Sv_moy_%d.png" %(path_png,x))

couleurs = plt.cm.jet(np.linspace(0,1,nombre_de_categories))
cmap = colors.ListedColormap(couleurs)  
  

# Affichage de la classification et de la réponse fréquentielle par clusters
fig,ax2 = plt.subplots(figsize=(18,12),dpi=80)
#image de classification
plt.subplot(211)
plt.xlabel('N*ESU')
plt.ylabel('profondeur (en m)')
plt.title(' ')
plt.imshow(np.transpose(labels),extent=[0,len(Sv),-Depth[1,(len(Depth[1])-1)],-Depth[1,0]], aspect='auto',cmap=cmap)
plt.grid()
    
plt.colorbar()
plt.show()
#réponse en fréquence
plt.subplot(212)
plt.xlabel('Freq')
plt.ylabel('Sv dB')
plt.title('Reponse en frequence par cluster')
for j in range(nombre_de_categories):
    
    # plt.plot(freq_MFR,kmeans.cluster_centers_[j],'H-.',color=couleurs[j])
    plt.plot(freq_MFR,Q1_[j],'-.',color=couleurs[j])
    plt.plot(freq_MFR,Q3_[j],'-.',color=couleurs[j])
    plt.plot(freq_MFR,med[j],'o-',color=couleurs[j])
    plt.grid()
fig.savefig("%s/figure_clustering.png"%path_png)   
    
#Affichage de Sv en fonction de la frequence     
Sv_affiche = []
freqs_affichage=[]
for x in range (len(freq_voulues)):
    indice = np.where(freq_MFR == freq_voulues[x])
    Sv_affiche.append(np.transpose(np.squeeze(Sv[:,:,indice])))
    freqs_affichage.append((freq_voulues[x])/1000)
Sv_affiche = np.array(Sv_affiche)
for x in range (len(nom_numero_radiales[1])):
    fig,ax3 = plt.subplots(figsize=(24,35))
    indices_rad = np.squeeze(np.where(tableau_radiales==nom_numero_radiales[1][x]))
    dr.DrawMultiEchoGram(Sv_affiche[:,:,indices_rad], Depth, len(freq_voulues),freqs = freqs_affichage, title = 'Sv sondeur %d kHz')
    fig.savefig("%s/figure_Sv_%d.png" %(path_png,x))
    plt.close()


# moyennage de Sa sur chaque frequence
for t in range(nb_transduc):
    Sa_moy = np.squeeze(Sa[:,:,t])
    
    # Suppression des valeurs n'ayant pas de sens dans la latitude et la longitude 
    index = np.where((Lon[:,0]==-200)&(Lat[:,0]==-100))
    
    Lat = np.delete(Lat,(index),axis=0)
    Lon = np.delete(Lon,(index),axis=0)
    
    labels =  np.delete(labels,(index),axis=0)
    Sa_moy = np.delete(Sa_moy,(index),axis=0)
    a=np.where(Sa_moy == 0)
    Sa_moy[a]=np.NaN
    
    # Calcul de Sa en logarithmique
    # Sa_moy_log = 10*np.log10(Sa_moy)
    # Sa_moy_log[a] = -100
    
    Lat_moy = np.true_divide(Lat.sum(1),(Lat!=0).sum(1))
    Lon_moy = np.true_divide(Lon.sum(1),(Lon!=0).sum(1))
    # Lat_moy = np.mean(Lat,axis=1)
    # Lon_moy = np.mean(Lon,axis=1)
    
    # Lon_moy_repmat = np.transpose(np.matlib.repmat(Lon_moy,len(Depth),1))
    # Lat_moy_repmat = np.transpose(np.matlib.repmat(Lat_moy,len(Depth),1))
    
    #définition des couches surface et fond agrégées pour relier au chalutage
    surface_limit=30
    indice_surface=int(np.floor((surface_limit-Depth[0,0])/(Depth[0,1]-Depth[0,0])))
    
    Sa_moy_label_surface = []   
    Sa_moy_label_fond = []   
    Sa_moy_label = []
    Sa_moy_label_log = []
    for x in range(nombre_de_categories):
            Sa_label = np.zeros(Sa_moy.shape)
            Sa_label[:,:]= np.nan
            index_lab  = np.where(labels==x)
            Sa_label[index_lab] = Sa_moy[index_lab]
            Sa_moy_label_surface.append(np.nansum(Sa_label[:,0:indice_surface],axis=1))
            Sa_moy_label_fond.append(np.nansum(Sa_label[:,indice_surface+1:],axis=1))
            Sa_moy_label.append(np.nansum(Sa_label,axis=1))     
            Sa_moy_label_log.append(10*np.log10(np.nanmean(Sa_label,axis=1)))
            

##################################################################################################
#
# Export de Sa (en valeurs naturelles et log), Lat et Lon dans un fichier CSV
#
#################################################################################################        
        
        
        
    for x in range(nombre_de_categories):
        with open('%s/Sa_surface_label_%d_freq_%d.csv' % (path_csv,x,freqs_moy_transducteur[t]), 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=' ')
            for i in range (len(Lat_moy)):
                if Sa_moy_label_surface[x][i]>0:
                    writer.writerow((Time[i].strftime("%d/%m/%Y %H:%M:%S"),Lat_moy[i],Lon_moy[i],Sa_moy_label_surface[x][i]))
                    
    for x in range(nombre_de_categories):
        with open('%s/Sa_fond_label_%d_freq_%d.csv' % (path_csv,x,freqs_moy_transducteur[t]), 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=' ')
            for i in range (len(Lat_moy)):
                if Sa_moy_label_fond[x][i]>0:
                    writer.writerow((Time[i].strftime("%d/%m/%Y %H:%M:%S"),Lat_moy[i],Lon_moy[i],Sa_moy_label_fond[x][i]))
                    
    for x in range(nombre_de_categories):
        with open('%s/Sa_label_%d_freq_%d.csv' % (path_csv,x,freqs_moy_transducteur[t]), 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=' ')
            for i in range (len(Lat_moy)):
                if Sa_moy_label[x][i]>0:
                    writer.writerow((Time[i].strftime("%d/%m/%Y %H:%M:%S"),Lat_moy[i],Lon_moy[i],Sa_moy_label[x][i]))
        
           

##################################################################################################
#
# Création de carte avec la librairie Cartopy : Carte simple , affichage dans les figures. 
# Sa en valeurs naturelles
#
##################################################################################################
 

md.affichage_carte_france(nombre_de_categories,labels,Lat_moy,Lon_moy,Sa_moy_label,path_png)
    

##################################################################################################
#
# Création de carte avec la librairie folium : Carte Openstreetmap, affichage dans un fichier HTML. 
# Sa en Log
#
##################################################################################################
 
md.save_map_html (nombre_de_categories,labels,Lat_moy,Lon_moy,Sa_moy_label_log,path_html)

##################################################################################################-

filelist = glob2.glob('%s/**/*.hac' % path_hac_survey)
if len(filelist) == 0:
    raise RuntimeError(path_hac_survey + ' does not contain HAC files')
                        
list_sounder =  au.hac_sounder_descr(filelist[0])
nb_snd=list_sounder.GetNbSounder()

with open('%s/rapport.adoc' % path_png, 'w') as rapport:
    
            rapport.write(("= RAPPORT \n\n"))
            rapport.write(("==== Descriptif des sondeurs \n\n"))
            rapport.write((' nb sondeurs = ' + str(nb_snd)+'\n'))
            for isdr in range(nb_snd):
                sounder = list_sounder.GetSounder(isdr)
                nb_transducs=sounder.m_numberOfTransducer
                rapport.write(( 'sondeur ' + str(isdr) + ':    index: ' + str(sounder.m_SounderId) + '   nb trans:' + str(nb_transducs)+'\n'))
                for itr in range(nb_transducs):
                    trans = sounder.GetTransducer(itr)      
                    for ibeam in range(trans.m_numberOfSoftChannel):
                        softChan = trans.getSoftChannelPolarX(ibeam)
                        rapport.write(( '   trans ' + str(itr) + ':    nom: ' + trans.m_transName + '   freq: ' + str(softChan.m_acousticFrequency/1000) + ' kHz\n'))
            
            rapport.write(('\n==== Etude de la classification multifrequence sur le sondeur EK80\n\n'))
            
            #  for x in range (len(nom_numero_radiales[1])):
            #     rapport.write("===== Sv moyen par transducteurs pour la radiale %s\n\n" %(nom_numero_radiales[0][x]))
            #     rapport.write(("image:figure_Sv_moy_%d.png[] \n\n" %(nom_numero_radiales[1][x])))
                
            for x in range (len(nom_numero_radiales[1])):
                rapport.write("===== Sv pour la radiale %s\n\n" %(nom_numero_radiales[0][x]))
                rapport.write(("image:figure_Sv_%d.png[] \n\n" %(nom_numero_radiales[1][x])))
                
            rapport.write("===== Resultat de classification\n\n")
            rapport.write("image:figure_clustering.png[] \n\n")
            
            for x in range (nombre_de_categories):
                rapport.write("===== Affichage du Sa pour le label %d\n\n" %x)
                rapport.write(("image:map_label_%d.png[] \n\n" %x))

sounder = list_sounder.GetSounder(0)                
# export.save_integrated_SonarNetCdf(sounder, Time, Sv, Lat, Lon, Depth, path_netcdf,survey_name)
