# -*- coding: utf-8 -*-
"""
Created on Wed Nov 25 09:05:45 2020

@author: lberger
|- import de librairies 
|
|- Sélection des limites horaires de l'analyse TS'
|
|- Initialisation des variables, des chemins d'accès aux données
|
|- Extraction des échos simples et concaténation dans un fichier pickle
|
|- Affichage des histogrammes de TS et de profondeur et du TS moyen par fréquence
|
|- Export au format csv


"""

import os
import pickle
import numpy as np
import matplotlib.pyplot as plt
import csv
import pymovies_3d.core.echointegration.ei_survey as ei
import pymovies_3d.core.TS_analysis.samplecomputeTS as TS
import pymovies_3d.core.TS_analysis.TS_bind as ts_bind

dateStart='10/10/2019'
timeStart='12:30:00'
dateEnd='10/10/2019'
timeEnd='13:30:00'



#path 
path_hac_survey = 'C:/Users/lberger/Desktop/GUERLEDAN/Calibration'
path_config = 'C:/Users/lberger/Desktop/GUERLEDAN/Configs_M3D/configsAnalyseM3D/TS'
survey_name='GUERLEDAN2019'


path_save = path_hac_survey + '/Result/' + survey_name +'/'

thresholds=[-60]

path_config=path_config + '/' + str(thresholds[0])

path_csv= path_save + 'csv/'
path_png= path_save + 'png/'

#If save path does not exist, create it
if not os.path.exists(path_save):
    os.makedirs(path_save)

if not os.path.exists(path_csv):
    os.makedirs(path_csv)

if not os.path.exists(path_png):
    os.makedirs(path_png)

    
indexSounder=0
indexTransducer=0

nameTransect='Calibration'

filename_EK80='%s/%s-TH%d-EK80-%s-%s-TS.pickle' % (path_save,survey_name,thresholds[0],indexSounder,indexTransducer)
    
   
var_choix = input('voulez vous modifier les données de TS?  \n   y or n \n')
if var_choix == 'y' :
    TS.sample_compute_TS(path_hac_survey,path_config,path_save,indexSounder,indexTransducer,nameTransect,dateStart,timeStart,dateEnd,timeEnd)
    #concaténation de tous des résultats d'analyse TS
    ts_bind.TS_bind(path_save,None,filename_EK80,None)

print('chargement des donnees...')
with open(filename_EK80,'rb') as f:
    timeTarget_all,TSrange_all,TScomp_all,TSucomp_all,TSalong_all,TSathwart_all,TSposition_all,TSpositionGPS_all,TSlabel_all,TSfreq_all = pickle.load(f)
    
TSvalues=np.squeeze(np.asarray(TScomp_all))
TSmean=np.mean(TSvalues,0)
TSposition=np.squeeze(np.asarray(TSpositionGPS_all))
TSfreq=np.squeeze(TSfreq_all[1])
    
#display TS en depth histograms and mean TS as a function of frequency
num_bins = 10
plt.figure(1,figsize=(30,15))
plt.clf()
plt.tight_layout()
# the histogram of TS
n, bins, patches = plt.hist(TSvalues, num_bins, normed=1)
plt.xlabel('TS (dB)')
plt.grid(True)
plt.show()

plt.figure(2,figsize=(30,15))
plt.clf()
plt.tight_layout()
# the histogram of the depth
n, bins, patches = plt.hist(TSposition[:,2], num_bins, normed=1, facecolor='blue', alpha=0.5)
plt.xlabel('Depth (m)')
plt.grid(True)
plt.show()

plt.figure(3,figsize=(30,15))
plt.clf()
plt.tight_layout()
# the mean TS
plt.plot(TSfreq/1000,TSmean)
plt.xlabel('Frequency (kHz)')
plt.ylabel('TS (dB)')
plt.grid(True)
plt.show()

#export csv
header = ['Time', 'Latitude', 'Longitude','Depth','TS (dB)','Label Track']
for f in range(len(TSfreq)):
    with open('%s/%s-TH%d-EK80-%s-%s-TS_freq_%dkHz.csv' % (path_csv,survey_name,thresholds[0],indexSounder,indexTransducer,TSfreq[f]), 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=' ')
        writer.writerow(h for h in header)
        for i in range (len(timeTarget_all)):
            writer.writerow((timeTarget_all[i].strftime("%d/%m/%Y %H:%M:%S"),TSposition[i,0],TSposition[i,1],TSposition[i,2],TSvalues[i,f],TSlabel_all[i]))
        

