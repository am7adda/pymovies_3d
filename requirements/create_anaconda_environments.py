# -*- coding: utf-8 -*-
"""
This module create all environments defined for the pyat project
return 0 if everything went OK or the error code value otherwise
"""

import argparse
import os
import platform
import sys
import tempfile
import shutil

# environment names as defined in the yml files
environment_runtime_name = 'pymovies_3D_runtime'
environment_test_name = 'pymovies_3D_test'
environment_dev_name = 'pymovies_3D_dev'

environment_dev_yml = "requirements_dev.yml"
environment_test_yml = "requirements_jenkins.yml"
environment_runtime_yml = "requirements_runtime.yml"


def process_exec(command: str) -> int:
    """
    Execute a command.
    exit process if an error occurred otherwise return 0

    """

    # execute command
    print("Executing :" + command)
    exit_code = os.system(command)
    # if linux, retrieve the associated error code
    if not platform.system() == 'Windows':
        exit_code = os.WEXITSTATUS(exit_code)
    # if an error occurred, exit all
    if exit_code > 0:
        print("Error while executing command:", command, " return code ", str(exit_code), file=sys.stderr)
        exit(exit_code)
    # always return 0
    return exit_code


def update_lib_path(conda_setup_directory, environment_name):
    if not platform.system() == 'Windows':
        conda_directory = conda_setup_directory + "/envs/" + environment_name
        """Update LD_LIBRARY_PATH for anaconda"""
        cmd = "cd " + conda_directory + " && mkdir -p ./etc/conda/activate.d " \
                                    "&& mkdir -p ./etc/conda/deactivate.d  " \
                                    "&& touch ./etc/conda/activate.d/env_vars.sh    " \
                                    "&& touch ./etc/conda/deactivate.d/env_vars.sh" \
                                    "&& echo 'export LD_LIBRARY_PATH_OLD=$LD_LIBRARY_PATH' > ./etc/conda/activate.d/env_vars.sh " \
                                    "&& echo 'export LD_LIBRARY_PATH=$CONDA_PREFIX/lib:$LD_LIBRARY_PATH' >> ./etc/conda/activate.d/env_vars.sh" \
                                    "&& echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH_OLD' > ./etc/conda/deactivate.d/env_vars.sh " \
                                    "&& echo 'unset LD_LIBRARY_PATH_OLD' >> ./etc/conda/deactivate.d/env_vars.sh"

        process_exec(cmd)


def create_for_runtime(conda_setup_directory):
    """ create environment for runtime targets"""
    # remove old environment
    process_exec("conda env remove --name " + environment_runtime_name)
    cleanup_env(conda_setup_directory, environment_runtime_name)

    # create runtime
    process_exec("conda env create -f " + environment_runtime_yml + " --name " + environment_runtime_name)
    update_lib_path(conda_setup_directory, environment_runtime_name)
    # everything was ok, return 0
    return 0


def create_for_test(conda_setup_directory):
    """ create environment for test targets, ie create runtime and test environment"""
    create_for_runtime(conda_setup_directory)
    # remove old environment
    process_exec("conda env remove --name " + environment_test_name)
    cleanup_env(conda_setup_directory, environment_test_name)
    # clone runtime environment
    process_exec("conda create -y --name " + environment_test_name + " --clone " + environment_runtime_name)
    # update environment with yaml file
    process_exec("conda env update --file " + environment_test_yml + " --name " + environment_test_name)
    update_lib_path(conda_setup_directory, environment_test_name)
    return 0


def cleanup_env(conda_path, env_name):
    """ sometimes conda keep .trash files in environment so try to clean it up"""
    dir_env = conda_path + "/envs/" + env_name
    if os.path.exists(dir_env):
        try:
            tempdir = tempfile.mkdtemp(suffix="old_env")
            shutil.move(dir_env, tempdir)
            print("Moving directory {} to {}".format(dir_env, tempdir))
        except OSError as e:
            print("An error occurred {}".format(e))


def create_for_dev(conda_setup_directory):
    """ create environment for development, ie create runtime, test and dev environments"""
    create_for_test(conda_setup_directory)
    # remove old environment
    process_exec("conda env remove --name " + environment_dev_name)
    cleanup_env(conda_setup_directory, environment_dev_name)
    # clone test environment
    process_exec("conda create -y --name " + environment_dev_name + " --clone " + str(environment_test_name))
    # update environment with yaml file
    process_exec("conda env update --file " + environment_dev_yml + " --name " + environment_dev_name)
    update_lib_path(conda_setup_directory, environment_dev_name)
    return 0


def clean_all(conda_setup_directory):
    """remove all previous environment"""
    process_exec("conda env remove --name " + environment_dev_name)
    process_exec("conda env remove --name " + environment_runtime_name)
    process_exec("conda env remove --name " + environment_test_name)
    process_exec("conda clean --all --yes ")
    cleanup_env(conda_setup_directory, environment_dev_name)
    cleanup_env(conda_setup_directory, environment_runtime_name)
    cleanup_env(conda_setup_directory, environment_test_name)


def exec(args):
    conda_setup_directory = get_conda_path()
    if 'runtime' in args.target:
        return create_for_runtime(conda_setup_directory)
    if 'dev' in args.target:
        return create_for_dev(conda_setup_directory)
    if 'test' in args.target:
        return create_for_test(conda_setup_directory)
    if 'clean' in args.target:
        return clean_all(conda_setup_directory)
    print("Unknown target, see help for more information", file=sys.stderr)
    return 1


def get_conda_path():
    # retrieve anaconda setup directory
    outputfile = tempfile.mktemp(suffix="anaconda_path.txt")
    process_exec("conda info > " + outputfile)
    conda_setup_directory = "c:\\Tools\\"
    # parse the file
    try:
        with open(outputfile, "r") as f:
            content = f.readlines()
            for line in content:
                if line.find("base environment") >= 0:
                    subparts = line.split(':')
                    rightpart = subparts[1:]
                    rightpart = ":".join(rightpart)
                    dir = rightpart.split('(')[0]
                    conda_setup_directory = dir.strip()
                    break
    except Exception as e:
        print("Unknown error while trying to retrieve conda path, continuing ", e)
    print("Find install dir for conda : {} ", conda_setup_directory)
    return conda_setup_directory


parser = argparse.ArgumentParser(description="recreate the anaconda pyat environment depending on the target platform")
parser.add_argument("-target", required=True, help="select the environment target (runtime,test,dev,clean)")
# parse arg, exit 2 if not defined
args = parser.parse_args()
# execute and interpret argument and exit with returned value
exit(exec(args))
