

Due to the python packaging constraints and the project file organization
a few operation are needed in order to be able to load your code and use it in notebooks


## Manage Import

If you want to write your function in src directory ```pyat/funct/Foo.py``` and use it in notebooks you need to import it

* One solution is to import the file in your notebooks like this.

```
from os import sys, path
# add the upper directory to known path
sys.path.append(path.dirname(path.dirname(path.abspath("__file__"))))
```

* Another preferred solution is to your `src` directory with your notebooks so that you can import it directly
run the following command in your conda environment and it will install your directory in your environment and

```
pip install --editable .
```
So this allows us to use the ./src folders provided with your project and easily import custom functions into your notebook.



## Tell jupyter to watch code changes

```

# OPTIONAL: Load the "autoreload" extension so that code can change
%load_ext autoreload

# OPTIONAL: always reload modules so that as you change code in src, it gets loaded
%autoreload 2

```


## References

https://medium.com/@rrfd/cookiecutter-data-science-organize-your-projects-atom-and-jupyter-2be7862f487e
